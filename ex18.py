import timeit


def function_a():
    print("Function A starts the execution:")
    print("Function A completes the execution:")


def function_b():
    print("Function B starts the execution")
    print("Function B completes the execution")


start_time = timeit.default_timer()
function_a()
print(timeit.default_timer() - start_time)

start_time = timeit.default_timer()
function_b()
print(timeit.default_timer() - start_time)

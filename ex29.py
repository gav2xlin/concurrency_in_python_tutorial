import multiprocessing


def spawn_process(num):
    print("This is process: %s" % num)


if __name__ == "__main__":
    process_jobs = []
    for i in range(3):
        p = multiprocessing.Process(target=spawn_process, args=(i,))
        process_jobs.append(p)
        p.start()
        p.join()

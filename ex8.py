from threading import Lock


class ExtendClass(set):
    def __init__(self, *args, **kwargs):
        self._lock = Lock()
        super(ExtendClass, self).__init__(*args, **kwargs)

    def add(self, elem):
        self._lock.acquire()
        try:
            super(ExtendClass, self).add(elem)
        finally:
            self._lock.release()

    def remove(self, elem):
        self._lock.acquire()
        try:
            super(ExtendClass, self).remove(elem)
        finally:
            self._lock.release()

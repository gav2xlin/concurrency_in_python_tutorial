import queue

q = queue.LifoQueue()

for i in range(8):
    q.put("item-" + str(i))

while not q.empty():
    print(q.get(), end=" ")

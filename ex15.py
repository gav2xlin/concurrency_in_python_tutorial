import threading
import queue

import time


def myqueue(mq):
    while not mq.empty():
        item = mq.get()
        if item is None:
            break
        print("{} removed {} from the queue".format(threading.current_thread(), item))
        mq.task_done()
        time.sleep(1)


q = queue.PriorityQueue()
for i in range(5):
    q.put(i, True)

for i in range(5):
    q.put(i, True)

threads = []
for i in range(2):
    thread = threading.Thread(target=myqueue, args=(q,))
    thread.start()
    threads.append(thread)

for thread in threads:
    thread.join()

# pip install pyfunctional

from functional import seq

result = (
    seq(1, 2, 3).map(lambda x: x * 2).filter(lambda x: x > 4).reduce(lambda x, y: x + y)
)

print("Result: {}".format(result))

import asyncio


async def my_operation(_future):
    await asyncio.sleep(2)
    _future.set_result("Future Completed")


loop = asyncio.get_event_loop()
future = asyncio.Future()
asyncio.ensure_future(my_operation(future))
try:
    loop.run_until_complete(future)
    print(future.result())
finally:
    loop.close()

import unittest


def fib(n):
    """
    Calculates the Fibonacci number

    >>> fib(0)
    0
    >>> fib(1)
    1
    >>> fib(10)
    55
    >>> fib(20)
    6765
    >>>
    """
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a


class FiboTest(unittest.TestCase):
    def setUp(self):
        print("This is run before our tests would be executed")

    def tearDown(self):
        print("This is run after the completion of execution of our tests")

    def test_fib(self):
        self.assertEqual(fib(0), 0)
        self.assertEqual(fib(1), 1)
        self.assertEqual(fib(5), 5)
        self.assertEqual(fib(10), 55)
        self.assertEqual(fib(20), 6765)


if __name__ == "__main__":
    unittest.main()

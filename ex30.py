import multiprocessing
import time


def non_daemon_process():
    print("starting my Process")
    time.sleep(8)
    print("ending my Process")


def daemon_process():
    while True:
        print("Hello")
        time.sleep(2)


if __name__ == "__main__":
    _non_daemon_process = multiprocessing.Process(target=non_daemon_process)
    _daemon_process = multiprocessing.Process(target=daemon_process)
    _daemon_process.daemon = True
    _non_daemon_process.daemon = False
    _daemon_process.start()
    _non_daemon_process.start()

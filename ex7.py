import threading
import random
import time


class DiningPhilosopher(threading.Thread):
    running = True

    def __init__(self, x_name, left_fork, right_fork):
        threading.Thread.__init__(self)
        self.name = x_name
        self.left_fork = left_fork
        self.right_fork = right_fork

    def run(self):
        while self.running:
            time.sleep(random.uniform(3, 13))
            print("%s is hungry." % self.name)
            self.dine()

    def dine(self):
        fork1, fork2 = self.left_fork, self.right_fork

        while self.running:
            fork1.acquire(True)
            locked = fork2.acquire(False)
            if locked:
                break
            fork1.release()
            print("%s swaps forks" % self.name)
            fork1, fork2 = fork2, fork1
        else:
            return

        self.dining()
        fork2.release()
        fork1.release()

    def dining(self):
        print("%s starts eating " % self.name)
        time.sleep(random.uniform(1, 10))
        print("%s finishes eating and now thinking." % self.name)


def dining_philosophers():
    forks = [threading.Lock() for n in range(5)]
    philosopher_names = ("1st", "2nd", "3rd", "4th", "5th")

    philosophers = [
        DiningPhilosopher(philosopher_names[i], forks[i % 5], forks[(i + 1) % 5])
        for i in range(5)
    ]

    random.seed()
    DiningPhilosopher.running = True

    for p in philosophers:
        p.start()
    time.sleep(30)

    DiningPhilosopher.running = False
    print(" It is finishing.")


dining_philosophers()

import multiprocessing


def print_records(_records):
    for record in _records:
        print("Name: {0}\nScore: {1}\n".format(record[0], record[1]))


def insert_record(record, _records):
    _records.append(record)
    print("A New record is added\n")


if __name__ == "__main__":
    with multiprocessing.Manager() as manager:
        records = manager.list([("Computers", 1), ("History", 5), ("Hindi", 9)])
        new_record = ("English", 3)

        p1 = multiprocessing.Process(target=insert_record, args=(new_record, records))
        p2 = multiprocessing.Process(target=print_records, args=(records,))
        p1.start()
        p1.join()
        p2.start()
        p2.join()

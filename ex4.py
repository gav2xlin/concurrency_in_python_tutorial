import threading
import time
import random


def thread_execution(i):
    print("Execution of Thread {} started\n".format(i))
    sleep_time = random.randint(1, 4)
    time.sleep(sleep_time)
    print("Execution of Thread {} finished".format(i))


for i in range(4):
    thread = threading.Thread(target=thread_execution, args=(i,))
    thread.start()
    print("Active Threads:", threading.enumerate())

import multiprocessing
import time


def child_process():
    print("Starting function")
    time.sleep(5)
    print("Finished function")


p = multiprocessing.Process(target=child_process)
p.start()
print("My Process has terminated, terminating main thread")
print("Terminating Child Process")
p.terminate()
print("Child Process successfully terminated")

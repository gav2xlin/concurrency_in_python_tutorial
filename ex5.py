import threading
import time


def non_daemon_thread():
    print("starting my thread")
    time.sleep(8)
    print("ending my thread")


def daemon_thread():
    while True:
        print("Hello")
        time.sleep(2)


if __name__ == "__main__":
    non_daemon_thread = threading.Thread(target=non_daemon_thread)
    daemon_thread = threading.Thread(target=daemon_thread)
    daemon_thread.setDaemon(True)
    daemon_thread.start()
    non_daemon_thread.start()

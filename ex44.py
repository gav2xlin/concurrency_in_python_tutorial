import asyncio


@asyncio.coroutine
def my_operation():
    print("First Coroutine")


loop = asyncio.get_event_loop()
try:
    loop.run_until_complete(my_operation())

finally:
    loop.close()

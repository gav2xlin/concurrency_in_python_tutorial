# pip install RxPY

from rx import Observable, Observer


def get_strings(observer):
    observer.on_next("Ram")
    observer.on_next("Mohan")
    observer.on_next("Shyam")
    observer.on_completed()


class PrintObserver(Observer):
    @staticmethod
    def on_next(value):
        print("Received {0}".format(value))

    @staticmethod
    def on_completed():
        print("Finished")

    @staticmethod
    def on_error(error):
        print("Error: {0}".format(error))


source = Observable.create(get_strings)
source.subscribe(PrintObserver())

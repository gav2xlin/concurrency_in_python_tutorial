import multiprocessing


def mng_na_sp(_using_ns):
    _using_ns.x += 5
    _using_ns.y *= 10


if __name__ == "__main__":
    manager = multiprocessing.Manager()
    using_ns = manager.Namespace()
    using_ns.x = 1
    using_ns.y = 1

    print("before", using_ns)
    p = multiprocessing.Process(target=mng_na_sp, args=(using_ns,))
    p.start()
    p.join()
    print("after", using_ns)

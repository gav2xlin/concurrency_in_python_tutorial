import asyncio
import time


async def task_ex(n):
    time.sleep(1)
    print("Processing {}".format(n))


async def generator_task():
    for i in range(10):
        asyncio.ensure_future(task_ex(i))
    print("Tasks Completed")
    asyncio.sleep(2)

loop = asyncio.get_event_loop()
loop.run_until_complete(generator_task())
loop.close()

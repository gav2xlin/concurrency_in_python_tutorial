import multiprocessing


def square(n):
    return n * n


if __name__ == "__main__":
    inputs = list(range(5))
    pool = multiprocessing.Pool(processes=4)
    p_outputs = pool.map(square, inputs)
    pool.close()
    pool.join()
    print("Pool :", p_outputs)

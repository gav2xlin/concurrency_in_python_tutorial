import queue as Q

p_queue = Q.PriorityQueue()

p_queue.put((2, "Urgent"))
p_queue.put((1, "Most Urgent"))
p_queue.put((10, "Nothing important"))
p_queue.put((5, "Important"))

while not p_queue.empty():
    item = p_queue.get()
    print("%s - %s" % item)

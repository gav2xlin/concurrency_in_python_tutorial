from threading import Lock


def lock_decorator(method):
    def new_deco_method(self, *args, **kwargs):
        with self._lock:
            return method(self, *args, **kwargs)

    return new_deco_method


class DecoratorClass(set):
    def __init__(self, *args, **kwargs):
        self._lock = Lock()
        super(DecoratorClass, self).__init__(*args, **kwargs)

    @lock_decorator
    def add(self, *args, **kwargs):
        return super(DecoratorClass, self).add(elem)

    @lock_decorator
    def remove(self, *args, **kwargs):
        return super(DecoratorClass, self).remove(elem)
